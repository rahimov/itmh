<?php

class Movie extends Model
{
    protected $table = '`movies`';

    /**
     * @return array
     */
    public function getList()
    {
        $sql = "SELECT movies.*, persons.name as director FROM $this->table
        LEFT JOIN `persons` ON movies.director_id=persons.id
        ORDER BY `id` DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        $movies = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($movies as &$movie) {
            $movie['actors'] = $this->getCollection('`movies_persons`', $movie['id'], 'LEFT JOIN persons ON movies_persons.person_id=persons.id');
            $movie['genres'] = $this->getCollection('`movies_genres`', $movie['id'], 'LEFT JOIN genres ON movies_genres.genre_id=genres.id');
            $movie['covers'] = $this->getCollection('`covers`', $movie['id']);
        }

        return $movies;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function getOne($id)
    {
        $sql = "SELECT *, DATE_FORMAT(release_date,'%d.%m.%Y') AS `releaseDate` FROM $this->table WHERE `id`='{$id}' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        $movie = $query->fetch(PDO::FETCH_ASSOC);
        if ($movie) {
            $movie['release_date'] = $movie['releaseDate'];

            //load actors
            $sql = "SELECT `person_id` FROM `movies_persons` WHERE `movie_id`='{$id}'";
            $query = $this->db->prepare($sql);
            $query->execute();
            $movie['actors'] = $query->fetchAll(PDO::FETCH_COLUMN);

            //load genres
            $sql = "SELECT `genre_id` FROM `movies_genres` WHERE `movie_id`='{$id}'";
            $query = $this->db->prepare($sql);
            $query->execute();
            $movie['genres'] = $query->fetchAll(PDO::FETCH_COLUMN);

            //load covers
            $sql = "SELECT `id`, `file` FROM `covers` WHERE `movie_id`='{$id}'";
            $query = $this->db->prepare($sql);
            $query->execute();
            $movie['covers'] = $query->fetchAll(PDO::FETCH_ASSOC);

            return $movie;
        } else {
            return false;
        }
    }

    /**
     * @param array $fields
     * @return int|string
     */
    public function save(array $fields)
    {
        $id = intval($fields['id']);
        unset($fields['id']);

        $arrayAllFields = array_keys($fields);
        $arrayForSet = array();

        $actorsArray = array();
        $genresArray = array();
        foreach ($arrayAllFields as $field) {
            switch ($field) {
                case 'release_date':
                    $date = explode('.', $fields[$field]);
                    $arrayForSet[] = $field . ' = "' . $date[2] . '-' . $date[1] . '-' . $date[0] . '"';
                    break;

                case 'actors':
                    $actorsArray = $fields[$field];
                    break;

                case 'genres':
                    $genresArray = $fields[$field];
                    break;

                default:
                    if (!empty($fields[$field])) {
                        $arrayForSet[] = $field . ' = "' . $fields[$field] . '"';
                    }
            }

        }
        unset($fields, $arrayAllFields);

        $strForSet = implode(', ', $arrayForSet);
        try {
            if ($id) {
                $sql = "UPDATE $this->table SET $strForSet WHERE `id` = $id";
            } else {
                $sql = "INSERT INTO $this->table SET $strForSet";
            }

            $query = $this->db->prepare($sql);
            $result = $query->execute();
            if (!$id) {
                $id = $this->db->lastInsertId();
            }

            if (!empty($actorsArray) && $result) {
                $this->deleteCollection("DELETE FROM `movies_persons` WHERE `movie_id` = $id");
                $this->saveCollection('`movies_persons`', $id, $actorsArray, '(`person_id`, `movie_id`)');
            }

            if (!empty($genresArray) && $result) {
                $this->deleteCollection("DELETE FROM `movies_genres` WHERE `movie_id` = $id");
                $this->saveCollection('`movies_genres`', $id, $genresArray, '(`genre_id`, `movie_id`)');
            }

            return $id;
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            exit();
        }
    }

}