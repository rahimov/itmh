<?php

class Cover extends Model
{
    protected $table = '`covers`';

    /**
     * @param $movieId
     * @param $file
     * @return bool|string
     */
    public function save($movieId, $file)
    {
        try {
            $sql = "INSERT INTO $this->table SET `movie_id`='{$movieId}', `file`='{$file}'";
            $query = $this->db->prepare($sql);
            $query->execute();

            return $this->db->lastInsertId();
        } catch (Exception $exc) {
            print $exc->__toString();

            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $id = intval($id);
        if ($id) {
            try {
                $sql = "DELETE FROM $this->table WHERE `id`='{$id}'";
                $query = $this->db->prepare($sql);
                $query->execute();

                return true;
            } catch (Exception $exc) {
                print $exc->__toString();

                return false;
            }
        }
    }
}