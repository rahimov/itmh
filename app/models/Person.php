<?php

class Person extends Model
{
    protected $table = '`persons`';

    /**
     * @param string $name
     * @return bool
     */
    public function add($name = '')
    {

        try {
            $sql = "INSERT INTO $this->table SET `name`='{$name}'";
            $query = $this->db->prepare($sql);
            $query->execute();

            return true;
        } catch (Exception $exc) {
            print $exc->__toString();

            return false;
        }
    }
}