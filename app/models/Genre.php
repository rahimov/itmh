<?php

class Genre extends Model
{
    protected $table = '`genres`';

    /**
     * @return array
     */
    public function getList()
    {
        $this->orderby = "ORDER BY `title` ASC";
        $genres = parent::getList();

        return $genres;
    }

    /**
     * @param string $title
     * @return bool
     */
    public function add($title = '')
    {

        try {
            $sql = "INSERT INTO $this->table SET `title`='{$title}'";
            $query = $this->db->prepare($sql);
            $query->execute();

            return true;
        } catch (Exception $exc) {
            print $exc->__toString();

            return false;
        }
    }

}