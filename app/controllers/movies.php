<?php

class Movies extends Controller
{
    public function __construct()
    {
        $this->model = $this->model('Movie');
    }

    public function add()
    {
        if (!empty($_POST) && isset($_POST['title'])) {
            $result = $this->model->save($_POST);
            if ($result) {
                header("Location: /movies/edit/{$result}");
            }
        }
        $countries = $this->model('Country')->getList();
        $persons = $this->model('Person')->getList();
        $genres = $this->model('Genre')->getList();
        $this->view('movies/add', ['countries' => $countries, 'persons' => $persons, 'genres' => $genres]);
    }

    public function delete($id = 0)
    {
        if ($id) {
            $this->model->delete($id);
        }
        header("Location: /");
    }

    public function edit($id = 0)
    {
        if (!$id) {
            header("Location: /");

            return;
        }
        if (!empty($_POST) && isset($_POST['title'])) {
            $result = $this->model->save($_POST);
            if ($result) {
                header("Location: /");
            }
        }
        $countries = $this->model('Country')->getList();
        $persons = $this->model('Person')->getList();
        $genres = $this->model('Genre')->getList();
        $this->view('movies/edit', [
            'movie'     => $this->model->getOne($id),
            'countries' => $countries,
            'persons'   => $persons,
            'genres'    => $genres
        ]);
    }
}