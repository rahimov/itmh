<?php

class Countries extends Controller
{
    public function __construct()
    {
        $this->model = $this->model('Country');
    }

    public function index()
    {
        $this->view('countries/index', ['countries' => $this->model->getlist()]);
    }

    public function add()
    {
        $name = '';

        if (!empty($_POST) && isset($_POST['name'])) {
            $name = filter_var(trim($_POST['name']), FILTER_SANITIZE_STRING);
            $result = $this->model->add($name);
            if ($result) {
                header("Location: /countries/");
            }
        }
        $this->view('countries/add', ['name' => $name]);
    }

    public function delete($id = 0)
    {
        if ($id) {
            $this->model->delete($id);
        }
        header("Location: /countries/");
    }
}