<?php

class Persons extends Controller
{
    public function __construct()
    {
        $this->model = $this->model('Person');
    }

    public function index()
    {
        $this->view('persons/index', ['persons' => $this->model->getList()]);
    }

    public function add()
    {
        $name = '';

        if (!empty($_POST) && isset($_POST['name'])) {
            $name = filter_var(trim($_POST['name']), FILTER_SANITIZE_STRING);
            $result = $this->model->add($name);
            if ($result) {
                header("Location: /persons/");
            }
        }
        $this->view('persons/add', ['name' => $name]);
    }

    public function delete($id = 0)
    {
        if ($id) {
            $this->model->delete($id);
        }
        header("Location: /persons/");
    }
}