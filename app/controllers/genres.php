<?php

class Genres extends Controller
{
    public function __construct()
    {
        $this->model = $this->model('Genre');
    }

    public function index()
    {
        $this->view('genres/index', ['genres' => $this->model->getList()]);
    }

    public function add()
    {
        $title = '';

        if (!empty($_POST) && isset($_POST['title'])) {
            $title = filter_var(trim($_POST['title']), FILTER_SANITIZE_STRING);
            $result = $this->model->add($title);
            if ($result) {
                header("Location: /genres/");
            }
        }
        $this->view('genres/add', ['title' => $title]);
    }

    public function delete($id = 0)
    {
        if ($id) {
            $this->model->delete($id);
        }
        header("Location: /genres/");
    }
}