<?php

class Home extends Controller
{
    public function index()
    {
        $movies = $this->model('Movie');
        $this->view('home/index', ['movies' => $movies->getList()]);
    }


}