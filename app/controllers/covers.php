<?php

class Covers extends Controller
{
    private $movieId;

    public function __construct()
    {
        $this->model = $this->model('Cover');
    }

    public function add()
    {
        $this->movieId = empty($_POST['movieid']) ? '' : intval($_POST['movieid']);

        if (!$result = $this->handleFile()) {
            return '{}';
        }

        $preview = array();
        $previewConfig = array();

        foreach ($result['files'] as $file) {
            if ($coverId = $this->model->save($this->movieId, $file)) {
                $preview[] = "<img src='/covers/{$this->movieId}/100x145/{$file}' class='file-preview-image'>";
                $previewConfig[] = ['width' => '100px', 'url' => '/covers/delete/' . $coverId];
            }
        }

        foreach ($result['uploaded'] as $file) {
            $this->createThumbnail($file, 100, 145);
            $this->createThumbnail($file, 150, 218);
        }
        echo json_encode([
            'initialPreview' => $preview,
            'initialPreviewConfig' => $previewConfig,
            'append' => true
        ]);
    }

    public function delete($id = 0)
    {
        if ($id) {
            $cover = $this->model->getOne($id);
            if ($this->model->delete($id)) {
                $targetDir = PUB_PATH . '/covers/' . $cover['movie_id'];
                unlink($targetDir . '/100x145/' . $cover['file']);
                rmdir($targetDir . '/100x145');

                unlink($targetDir . '/150x218/' . $cover['file']);
                rmdir($targetDir . '/150x218');

                unlink($targetDir . '/' . $cover['file']);
                rmdir($targetDir);

                echo '{}';
            }
        }
    }

    public function handleFile()
    {
        if (empty($_FILES['covers'])) {
            return json_encode(['error' => 'No files found for upload.']);
        }

        // get the files posted
        $images = $_FILES['covers'];

        // a flag to see if everything is ok
        $success = null;

        // file paths to store
        $paths = [];
        $preview = [];
        $files = [];

        // get file names
        $filenames = $images['name'];

        // loop and process files
        for ($i = 0; $i < count($filenames); $i++) {
            $ext = explode('.', basename($filenames[$i]));

            $targetDir = PUB_PATH . '/covers/' . $this->movieId;
            $target = $targetDir . DIRECTORY_SEPARATOR . md5(uniqid()) . '.' . array_pop($ext);

            if (!is_dir($targetDir)) {
                mkdir($targetDir, 0755, true);
            }

            if (move_uploaded_file($images['tmp_name'][$i], $target)) {
                $success = true;
                $paths[] = $target;
                $preview[] = '/covers/' . $this->movieId . '/' . basename($target);
                $files[] = basename($target);
            } else {
                $success = false;
                break;
            }
        }

        // check and process based on successful status
        if ($success === true) {
            $output = ['uploaded' => $paths, 'files' => $files];

        } elseif ($success === false) {
            $output = ['error' => 'Error while uploading images. Contact the system administrator'];
            // delete any uploaded files
            foreach ($paths as $file) {
                unlink($file);
            }
        } else {
            $output = ['error' => 'No files were processed.'];
        }

        return $output;
    }

    public function saveToDb()
    {

    }

    public function createThumbnail($path, $width, $height)
    {
        $info = getimagesize($path); //получаем размеры картинки и ее тип
        $size = array($info[0], $info[1]); //закидываем размеры в массив

        //В зависимости от расширения картинки вызываем соответствующую функцию
        if ($info['mime'] == 'image/png') {
            $src = imagecreatefrompng($path); //создаём новое изображение из файла
        } else {
            if ($info['mime'] == 'image/jpeg') {
                $src = imagecreatefromjpeg($path);
            } else {
                if ($info['mime'] == 'image/gif') {
                    $src = imagecreatefromgif($path);
                } else {
                    return false;
                }
            }
        }

        $thumb = imagecreatetruecolor($width,
            $height); //возвращает идентификатор изображения, представляющий черное изображение заданного размера
        $src_aspect = $size[0] / $size[1]; //отношение ширины к высоте исходника
        $thumb_aspect = $width / $height; //отношение ширины к высоте аватарки

        if ($src_aspect < $thumb_aspect) {
            //узкий вариант (фиксированная ширина)
            $scale = $width / $size[0];
            $new_size = array($width, $width / $src_aspect);
            $src_pos = array(
                0,
                ($size[1] * $scale - $height) / $scale / 2
            ); //Ищем расстояние по высоте от края картинки до начала картины после обрезки
        } else {
            if ($src_aspect > $thumb_aspect) {
                //широкий вариант (фиксированная высота)
                $scale = $height / $size[1];
                $new_size = array($height * $src_aspect, $height);
                $src_pos = array(
                    ($size[0] * $scale - $width) / $scale / 2,
                    0
                ); //Ищем расстояние по ширине от края картинки до начала картины после обрезки
            } else {
                //другое
                $new_size = array($width, $height);
                $src_pos = array(0, 0);
            }
        }

        $new_size[0] = max($new_size[0], 1);
        $new_size[1] = max($new_size[1], 1);

        //Копирование и изменение размера изображения с ресемплированием
        imagecopyresampled($thumb, $src, 0, 0, $src_pos[0], $src_pos[1], $new_size[0], $new_size[1], $size[0],$size[1]);

        $targetDir = PUB_PATH . '/covers/' . $this->movieId . '/' . $width . 'x' . $height;
        $target = $targetDir . '/' . basename($path);

        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0755, true);
        }

        return imagejpeg($thumb, $target, 90);//Сохраняет JPEG/PNG/GIF изображение

    }
}