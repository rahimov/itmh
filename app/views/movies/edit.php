<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Редактирование фильма</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <link rel="stylesheet" href="/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="/css/bootstrap-multiselect.css">
    <link rel="stylesheet" href="/css/fileinput.min.css">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Каталог</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class=""><a href="/countries/">Страны</a></li>
                <li class=""><a href="/persons/">Люди</a></li>
                <li class=""><a href="/genres/">Жанры</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container">
    <div class="page-header">
        <h1>Редактирование фильма</h1>
    </div>

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="movieTab" style="margin-bottom: 15px;">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Основные параметры</a></li>
            <li role="presentation"><a href="#covers" aria-controls="covers" role="tab" data-toggle="tab">Обложки</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                <form class="form-horizontal" method="post">
                    <input type="hidden" name="id" value="<?=$data['movie']['id']?>" id="movieId">
                    <div class="form-group">
                        <label for="movieTitle" class="col-sm-3 control-label">Название фильма</label>
                        <div class="col-sm-9">
                            <input name="title" type="text" class="form-control" id="movieTitle" placeholder="Введите название" value="<?=$data['movie']['title']?>" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieTitleOriginal" class="col-sm-3 control-label">Оригинальное название фильма</label>
                        <div class="col-sm-9">
                            <input name="title_original" type="text" class="form-control" id="movieTitleOriginal" placeholder="Введите название" value="<?=$data['movie']['title_original']?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieCountry" class="col-sm-3 control-label">Страна</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="country_id" id="movieCountry" style="width: 300px;">
                                <option value="0"></option>
                                <?php foreach($data['countries'] as $country):?>
                                    <option value="<?=$country['id']?>" <?php if($country['id']==$data['movie']['country_id']) echo 'selected';?>><?=$country['name']?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieYearStart" class="col-sm-3 control-label">Год начала съемок</label>
                        <div class="col-sm-9">
                            <input name="year_start" type="text" class="form-control" id="movieYearStart" style="width: 100px;" value="<?=$data['movie']['year_start']?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieYearEnd" class="col-sm-3 control-label">Год окончания съемок</label>
                        <div class="col-sm-9">
                            <input name="year_end" type="text" class="form-control" id="movieYearEnd" style="width: 100px;" value="<?=$data['movie']['year_end']?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieDirector" class="col-sm-3 control-label">Режиссёр</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="director_id" id="movieDirector" style="width: 300px;">
                                <option value="0"></option>
                                <?php foreach($data['persons'] as $person):?>
                                    <option value="<?=$person['id']?>" <?php if($person['id']==$data['movie']['director_id']) echo 'selected';?>><?=$person['name']?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieActors" class="col-sm-3 control-label">Актеры</label>
                        <div class="col-sm-9">
                            <select class="form-control multiple" name="actors[]" id="movieActors" style="width: 300px;" multiple="multiple" >
                                <option value="0"></option>
                                <?php foreach($data['persons'] as $person):?>
                                    <option value="<?=$person['id']?>" <?php if(in_array($person['id'],$data['movie']['actors'])) echo 'selected';?>><?=$person['name']?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieDuration" class="col-sm-3 control-label">Продолжительность (сек.)</label>
                        <div class="col-sm-9">
                            <input name="duration" type="text" class="form-control" id="movieDuration" style="width: 200px;"  value="<?=$data['movie']['duration']?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieRelease" class="col-sm-3 control-label">Дата премьеры</label>
                        <div class="col-sm-9">
                            <input name="release_date" type="text" class="form-control datepicker" id="movieRelease" data-date-format="dd.mm.yyyy" style="width: 200px;" value="<?=$data['movie']['release_date']?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieGenre" class="col-sm-3 control-label">Жанр</label>
                        <div class="col-sm-9">
                            <select class="form-control multiple" name="genres[]" id="movieGenre" style="width: 300px;" multiple="multiple" >
                                <option value="0"></option>
                                <?php foreach($data['genres'] as $genre):?>
                                    <option value="<?=$genre['id']?>" <?php if(in_array($genre['id'],$data['movie']['genres'])) echo 'selected';?>><?=$genre['title']?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieIntroText" class="col-sm-3 control-label">Анонс</label>
                        <div class="col-sm-9">
                            <textarea name="introtext" class="form-control" rows="5" id="movieIntroText"><?=$data['movie']['introtext']?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="movieText" class="col-sm-3 control-label">Описание</label>
                        <div class="col-sm-9">
                            <textarea name="text" class="form-control" rows="10" id="movieText"><?=$data['movie']['text']?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">Сохранить</button>
                            <a class="btn btn-default" href="/" role="button">Отмена</a>
                        </div>
                    </div>

                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="covers">
                <input id="fileupload" type="file" name="covers[]" class="file-loading" accept="image/*" multiple>

            </div>
        </div>
    </div>



</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/bootstrap-datepicker.ru.min.js"></script>
<script src="/js/bootstrap-multiselect.js"></script>

<script src="/js/fileinput.min.js"></script>
<script src="/js/fileinput_locale_ru.js"></script>
<script>
    <?php
        $coversPreview = array();
        $coversPreviewConfig = array();
        foreach($data['movie']['covers'] as $cover){
            $coversPreview[] = '<img src="/covers/'.$data['movie']['id'].'/100x145/'.$cover['file'].'">';
            $coversPreviewConfig[] = array('width'=> '100px', 'url' => '/covers/delete/'.$cover['id']);
        }
        echo 'var coversPreview = '.json_encode($coversPreview).';'.PHP_EOL;
        echo 'var coversPreviewConfig ='.json_encode($coversPreviewConfig).';'.PHP_EOL;
    ?>
</script>

<script>
    $(function () {
        $('.datepicker').datepicker({
            language: "ru",
            todayHighlight: true
        });

        $('.multiple').multiselect({
            nonSelectedText: 'Не выбрано',
            maxHeight: 200
        });

        $('#movieTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $inputUpload = $('#fileupload');
        $inputUpload.fileinput({
            uploadUrl: '/covers/add',
            uploadAsync: false,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
            minFileCount: 1,
            maxFileCount: 5,
            uploadExtraData: function() {
                return {
                    movieid: $('#movieId').val()
                };
            },
            previewFileType: "image",
            browseClass: "btn btn-success",
            browseLabel: "Выберите изображения",
            browseIcon: '<i class="glyphicon glyphicon-picture"></i> ',
            overwriteInitial: false,
            initialPreview: coversPreview,
            initialPreviewConfig: coversPreviewConfig
        }).on('filebatchselected', function(event, files) {
            // trigger upload method immediately after files are selected
            $inputUpload.fileinput('upload');
        });

    });
</script>
</body>
</html>