<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Movies</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Каталог</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class=""><a href="/countries/">Страны</a></li>
                <li class=""><a href="/persons/">Люди</a></li>
                <li class=""><a href="/genres/">Жанры</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container">
    <div class="page-header">
        <h1>Фильмы</h1>
    </div>
    <a class="btn btn-default" href="/movies/add" role="button">Добавить фильм</a><br><br>

    <?php foreach($data['movies'] as $movie):?>
        <div class="well">
            <div class="row">
                <div class="col-sm-2">
                    <?php foreach($movie['covers'] as $cover):?>
                        <a href="/covers/<?=$movie['id']?>/150x218/<?=$cover['file']?>" data-original="/covers/<?=$movie['id']?>/<?=$cover['file']?>"><img src="/covers/<?=$movie['id']?>/100x145/<?=$cover['file']?>"></a>
                    <?php endforeach;?>
                </div>
                <div class="col-sm-10">
                    <h4><?=$movie['title']?></h4>
                    <i>
                        <?php if(!empty($movie['title_original'])): ?><?=$movie['title_original']?><?php endif;?>
                        <?php if(!empty($movie['year_end'])): ?>(<?=$movie['year_end']?>)<?php endif;?>
                    </i>
                    <p>
                        <?php foreach($movie['genres'] as $genre):?>
                            <span class="label label-default"><?=$genre['title']?></span>
                        <?php endforeach;?>
                    </p>
                    <?php if(!empty($movie['director'])): ?>
                        <p>Режиссёр: <a><?=$movie['director']?></a></p>
                    <?php endif;?>
                    <?php if(!empty($movie['actors'])): ?>
                    <p>В ролях:
                        <?php foreach($movie['actors'] as $actor):?>
                            <a><?=$actor['name']?></a>&nbsp;
                        <?php endforeach;?>
                    </p>
                    <?php endif;?>
                    <p><?=$movie['introtext']?></p>
                    <p><a href="/movies/edit/<?=$movie['id']?>">Редактировать</a> <a href="/movies/delete/<?=$movie['id']?>">Удалить</a></p>
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
