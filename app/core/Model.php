<?php

class Model
{
    protected $db;
    protected $table;
    protected $orderby;

    public function __construct()
    {
        $this->db = $this->dbConnect();
        $this->orderby = "ORDER BY `name` ASC";
    }

    /**
     * @return PDO
     */
    protected function dbConnect()
    {
        try {
            $db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
            $db->exec('SET CHARACTER SET utf8');
            $this->mysql_connect = $db;
        } catch (PDOException $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
            exit;
        }

        return $this->mysql_connect;
    }

    /**
     * @return array
     */
    public function getList()
    {
        try {
            $sql = "SELECT * FROM $this->table $this->orderby";
            $query = $this->db->prepare($sql);
            $query->execute();

            $rows = $query->fetchAll(PDO::FETCH_ASSOC);

            return $rows;
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            exit();
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function getOne($id)
    {
        try {
            $sql = "SELECT * FROM $this->table WHERE `id`='{$id}' LIMIT 1";
            $query = $this->db->prepare($sql);
            $query->execute();

            $row = $query->fetch(PDO::FETCH_ASSOC);

            return $row;
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            exit();
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $id = intval($id);
        if ($id) {
            try {
                $sql = "DELETE FROM $this->table WHERE `id`='{$id}'";
                $query = $this->db->prepare($sql);
                $query->execute();

                return true;
            } catch (PDOException $e) {
                echo 'Error : ' . $e->getMessage();
                exit();
            }
        }
    }

    /**
     * @param $table
     * @param $id
     * @param string $join
     * @return array
     */
    public function getCollection($table, $id, $join = '')
    {
        try {

            $sql = "SELECT * FROM $table $join WHERE `movie_id`=$id";
            $query = $this->db->prepare($sql);
            $query->execute();

            $rows = $query->fetchAll(PDO::FETCH_ASSOC);

            return $rows;
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            exit();
        }
    }

    /**
     * @param $table
     * @param $id
     * @param $fields
     * @param $columns
     * @return bool
     */
    public function saveCollection($table, $id, $fields, $columns)
    {
        $arrayForSet = array();
        foreach ($fields as $field) {
            $arrayForSet[] = "('{$field}','{$id}')";
        }

        try {
            $sql = "INSERT INTO $table $columns VALUES " . implode(',', $arrayForSet);

            $query = $this->db->prepare($sql);
            $result = $query->execute();

            return true;
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            exit();
        }
    }

    /**
     * @param $select
     * @return bool
     */
    public function deleteCollection($select)
    {
        try {
            $result = $this->db->exec($select);

            return true;
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            exit;
        }
    }
}