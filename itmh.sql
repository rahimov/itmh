-- phpMyAdmin SQL Dump
-- version 4.2.10.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 25 2015 г., 14:36
-- Версия сервера: 5.5.41-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `itmh`
--

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `countries`
--

INSERT INTO `countries` (`id`, `name`) VALUES
(1, 'Россия'),
(2, 'Франция'),
(3, 'Бельгия'),
(4, 'США'),
(5, 'Великобритания'),
(7, 'Новая Зеландия');

-- --------------------------------------------------------

--
-- Структура таблицы `covers`
--

CREATE TABLE IF NOT EXISTS `covers` (
`id` int(10) unsigned NOT NULL,
  `movie_id` int(10) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `covers`
--

INSERT INTO `covers` (`id`, `movie_id`, `file`) VALUES
(12, 2, '73867e4b1cacd94e5bae0e68e2b3cc61.jpg'),
(13, 2, '7ca5a815f2fa6f52ca3a89ff2d59ae29.jpg'),
(14, 4, '951139a58d306ba39b98f9188d8c5648.jpg'),
(15, 5, '2fc37c7b18e6462d9e6a53d3fd2e2e23.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `genres`
--

CREATE TABLE IF NOT EXISTS `genres` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genres`
--

INSERT INTO `genres` (`id`, `title`) VALUES
(1, 'Фантастика'),
(2, 'Драма'),
(3, 'Приключения'),
(4, 'Семейный'),
(5, 'Фентези');

-- --------------------------------------------------------

--
-- Структура таблицы `movies`
--

CREATE TABLE IF NOT EXISTS `movies` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_original` varchar(255) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `year_start` year(4) DEFAULT NULL,
  `year_end` year(4) DEFAULT NULL,
  `director_id` int(10) DEFAULT NULL,
  `duration` int(5) DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `introtext` text NOT NULL,
  `text` text NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `publishedon` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `movies`
--

INSERT INTO `movies` (`id`, `title`, `title_original`, `country_id`, `year_start`, `year_end`, `director_id`, `duration`, `release_date`, `introtext`, `text`, `published`, `publishedon`) VALUES
(1, 'Тест', 'Test second', 5, 2013, 2014, 1, 6500, '2015-01-15', 'Интро', 'Текст', NULL, NULL),
(2, 'Хоббит: Битва пяти воинств', 'The Hobbit: The Battle of the Five Armies', 7, 2014, 2014, 1, 7200, '2015-02-01', 'Когда отряд из тринадцати гномов нанимал хоббита Бильбо Бэгинса в качестве взломщика и четырнадцатого, «счастливого», участника похода к Одинокой горе, Бильбо полагал, что его приключения закончатся, когда он выполнит свою задачу — найдет сокровище, которое так необходимо предводителю гномов Торину. Путешествие в Эребор, захваченное драконом Смаугом королевство гномов, оказалось еще более опасным, чем предполагали гномы и даже Гэндальф — мудрый волшебник, протянувший Торину и его отряду руку помощи.', 'Когда отряд из тринадцати гномов нанимал хоббита Бильбо Бэгинса в качестве взломщика и четырнадцатого, «счастливого», участника похода к Одинокой горе, Бильбо полагал, что его приключения закончатся, когда он выполнит свою задачу — найдет сокровище, которое так необходимо предводителю гномов Торину. Путешествие в Эребор, захваченное драконом Смаугом королевство гномов, оказалось еще более опасным, чем предполагали гномы и даже Гэндальф — мудрый волшебник, протянувший Торину и его отряду руку помощи.\r\n\r\nВ погоню за гномами устремилась армия орков, ведомых пробудившимся в руинах древним злом, а эльфы и люди, с которыми Бильбо и его товарищам пришлось иметь дело во время путешествия и которые пострадали от последствий желания гномов вернуть свой дом, предъявили права на щедрое вознаграждение — часть сокровищ Одинокой горы. Скоро неподалеку от Одинокой горы встретятся пять армий, и лишь кровопролитная битва определит результаты смелого гномьего похода.', NULL, NULL),
(4, 'Интерстеллар', 'Interstellar', 4, 2013, 2014, 6, 7200, '2014-10-10', 'Когда засуха приводит человечество к продовольственному кризису, коллектив исследователей и учёных отправляется сквозь червоточину (которая предположительно соединяет области пространства-времени через большое расстояние) в путешествие, чтобы превзойти прежние ограничения для космических путешествий человека и переселить человечество на другую планету.', 'Когда засуха приводит человечество к продовольственному кризису, коллектив исследователей и учёных отправляется сквозь червоточину (которая предположительно соединяет области пространства-времени через большое расстояние) в путешествие, чтобы превзойти прежние ограничения для космических путешествий человека и переселить человечество на другую планету.', NULL, NULL),
(5, 'Голодные игры: Сойка-пересмешница. Часть I', 'The Hunger Games: Mockingjay - Part 1', 5, 2014, 2015, 2, 7200, '2015-02-01', '75-ые Голодные игры изменили все. Китнисс нарушила правила, и непоколебимое до той поры деспотичное правление Капитолия пошатнулось. У людей появилась надежда, и ее символ — Сойка-пересмешница. ', '75-ые Голодные игры изменили все. Китнисс нарушила правила, и непоколебимое до той поры деспотичное правление Капитолия пошатнулось. У людей появилась надежда, и ее символ — Сойка-пересмешница. Теперь, чтобы освободить захваченного в плен Пита и защитить своих близких, Китнисс придется сражаться в настоящих битвах и стать еще сильнее, чем на арене игр.', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `movies_genres`
--

CREATE TABLE IF NOT EXISTS `movies_genres` (
  `genre_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `movies_genres`
--

INSERT INTO `movies_genres` (`genre_id`, `movie_id`) VALUES
(1, 1),
(1, 4),
(1, 5),
(2, 1),
(2, 4),
(3, 2),
(3, 5),
(4, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `movies_persons`
--

CREATE TABLE IF NOT EXISTS `movies_persons` (
  `person_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `movies_persons`
--

INSERT INTO `movies_persons` (`person_id`, `movie_id`) VALUES
(3, 1),
(3, 5),
(4, 5),
(7, 4),
(8, 4),
(9, 1),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `persons`
--

INSERT INTO `persons` (`id`, `name`) VALUES
(1, 'Питер Джексон'),
(2, 'Френсис Лоуренс'),
(3, 'Дженнифер Лоуренс'),
(4, 'Лиам Хемсворт'),
(5, 'Джош Хатчерсон'),
(6, 'Кристофер Нолан'),
(7, 'Мэттью МакКонахи'),
(8, 'Энн Хэтэуэй'),
(9, 'Джессика Честейн'),
(10, 'Мартин Фриман'),
(11, 'Иэн МакКеллен'),
(12, 'Ричард Армитедж'),
(13, 'Джеймс Несбитт'),
(14, 'Кен Стотт'),
(15, 'Уэс Бентли');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `covers`
--
ALTER TABLE `covers`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `genres`
--
ALTER TABLE `genres`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `movies`
--
ALTER TABLE `movies`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_country_id` (`country_id`);

--
-- Индексы таблицы `movies_genres`
--
ALTER TABLE `movies_genres`
 ADD PRIMARY KEY (`genre_id`,`movie_id`), ADD KEY `fk_genre_id` (`genre_id`);

--
-- Индексы таблицы `movies_persons`
--
ALTER TABLE `movies_persons`
 ADD PRIMARY KEY (`person_id`,`movie_id`), ADD KEY `fk_person_id` (`person_id`);

--
-- Индексы таблицы `persons`
--
ALTER TABLE `persons`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `covers`
--
ALTER TABLE `covers`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `genres`
--
ALTER TABLE `genres`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `movies`
--
ALTER TABLE `movies`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `persons`
--
ALTER TABLE `persons`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
